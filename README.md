# Airin Technical Test (Web/JS Developer) - Taylor Hogge

![Screenshot](/screenshot.png)

# Technologies Used
## API
### PostgreSQL

  I chose [PostgreSQL](https://www.postgresql.org/) for the database because it
  is open source, has nice built in search capabilities, and can be less error
  prone than working with a schemaless database.

### TypeORM

  I chose to use [TypeORM](https://github.com/typeorm/typeorm) to communicate
  with the postgres database because it turns stringly typed SQL queries into
  strongly typed Typescript code. This helps prevent a lot of bugs. TypeORM also
  automatically generates a database schema directly from your Typescript classes.

### ExpressJS

  I chose [ExpressJS](http://expressjs.com/) mainly because it was the only Node
  HTTP server I was familiar with. There are also some nice looking
  [tools](https://www.freecodecamp.org/news/express-js-and-aws-lambda-a-serverless-love-story-7c77ba0eaa35/)
  that can deploy Express servers to AWS Lambda.

### Argon2

  I chose to hash passwords with
  [Argon2](https://github.com/ranisalt/node-argon2) because it seems to be the
  standard choice among Node servers.

## Client
### Tachyons

  I chose to use [Tachyons](http://tachyons.io/) because it is useful for
  quickly blocking out a decent looking website. I didn't think I would have
  time to write a bunch of custom CSS so I grabbed tachyons to get some
  fundamental building blocks.
  
# Running Locally
## Database

  Please follow a guide for installing and running PostgreSQL. [This
  one](https://www.codementor.io/engineerapart/getting-started-with-postgresql-on-mac-osx-are8jcopb)
  is the one I followed to get it up and running on my Mac.
  
  Once you have Postgres up and running create a database for use by the app.

      $ createdb airin
  
## API

  Now that the database is ready we need to configure the api to use it.
  
  Start by installing the api packages.
  
      $ cd api
      $ npm install
      
  Now we need to create a .env file to tell the server how to login to the database
  
      $ touch .env
      
  Open this file in a text editor and define the following variables:
  
      DB_HOST=localhost
      DB_PORT=5432
      DB_USER=<your-database-username>
      DB_PASS=<your-database-password>
      DB_NAME=airin
      SERVER_PORT=8080
      
  Now we should be able to run the server and it will set up our database tables
  
      $ npm run start

## Client

  Now that the api is running we can start the client.
  
  Start by installing the client packages.
  
      $ cd client
      $ npm install
      
  Note: If you used a different server port than 8080 you will have to edit the
  ApiService class in client/src/app/api.service.ts
  Make sure that getBaseUrl() in ApiService returns the correct url to the api.
  
      public getBaseUrl(): string {
          return 'http://localhost:8080';
      }
      
  If that looks correct, go ahead and start the web client.
  Assuming you have the angular-cli installed:
  
      $ ng serve --open
      
  Initially there will be no data in the database, so register for an account by
  clicking sign in and then ask some questions and write some answers to
  generate some data.
  
  Feel free to [email me](mailto:hoggetaylor@gmail.com) if there are issues
  getting this running.
      
# Further Work

  Had I had time to work on this project more, these are the items that could
  use some attention:
  
  - Everything really really needs unit tests.
  - Generate OpenAPI definitions from Typescript controllers.
  - Share type definitions between client and api.
  - Set up CI to test and deploy the application.
  - Configure the environment correctly in the Angular client so that you dont have
    to manually change the api base URL.
  - Implement the rest of the functionality provided by the API in the client.
    The api provides functionality for deleting content that the client does not support.
    
# Retrospective

  In retrospect, I think I underestimated how much work adding users to this
  simple app would take. Had I not done this I would have probably had time to
  focus on other things that would have made the project a bit cleaner like
  tests/CI and hosting. This was one of my first times using Typescript/Node on
  the backend and Angular2 on the frontend but I think I got a grip on their
  techniques. I could definitely benefit from more practice with each of those
  technologies though.
