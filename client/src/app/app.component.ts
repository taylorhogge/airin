import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from './session.service';
import { SnackbarService } from './snackbar.service';

/**
 * This class defines the main component for our application.
 * Displays the navbar and bottom snackbar with the content of the current page
 * placed between.
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(
    private sessionService: SessionService,
    private snackbarService: SnackbarService,
    private router: Router
  ) {}

  /**
   * Determines what text is displayed in the main link in the nav bar.
   */
  getMainLinkText(): string {
    if (this.sessionService.isLoggedIn()) {
      return this.sessionService.getUsername() + ' - Log Out';
    } else {
      return 'Sign In';
    }
  }

  /**
   * Determines what happens when the main linked is clicked.
   * If the user is logged in, this button logs them out.
   * If the user is not logged in, it links them to the login page.
   */
  mainLinkClicked() {
    if (this.sessionService.isLoggedIn()) {
      this.sessionService.logout();
      this.snackbarService.showSnackbar('Successfully logged out');
      this.router.navigate(['/']);
    } else {
      this.router.navigate(['/login']);
    }
  }

  /**
   * Returns true if the user is currently logged in.
   */
  isLoggedIn(): boolean {
    return this.sessionService.isLoggedIn();
  }

}
