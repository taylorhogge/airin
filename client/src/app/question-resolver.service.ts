import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Question, QuestionService } from './question.service';

/**
 * This class defines the resolver used by the "/question:id" page
 * to resolve questions by their id.
 */
@Injectable({
  providedIn: 'root'
})
export class QuestionResolverService implements Resolve<Question> {

  constructor(
    private questionService: QuestionService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Question> | Observable<never> {
    const id = +route.paramMap.get('id');
    return this.questionService.getQuestionById(id);
  }
}
