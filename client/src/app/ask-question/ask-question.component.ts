import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Question, QuestionService } from '../question.service';
import { SessionService } from '../session.service';
import { SnackbarService } from '../snackbar.service';

interface FormData {
  title: string;
  body: string;
}

/**
 * This component defines what is displayed on the "/ask" page.
 * Contains a form that can be used to ask a new question.
 * If the user is not logged in upon entering this page, they will
 * be redirected to the home page.
 */
@Component({
  selector: 'app-ask-question',
  templateUrl: './ask-question.component.html',
  styleUrls: ['./ask-question.component.css']
})
export class AskQuestionComponent implements OnInit {

  questionForm: FormGroup;
  submittedOnce: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private sessionService: SessionService,
    private questionService: QuestionService,
    private snackbarService: SnackbarService,
    private router: Router
  ) {
    this.questionForm = this.formBuilder.group({
      title: new FormControl(null, [Validators.required]),
      body: new FormControl(null, [Validators.required])
    });
    this.submittedOnce = false;
  }

  ngOnInit() {
    // Redirect to home if the user is not logged in.
    if (!this.sessionService.isLoggedIn()) {
      this.router.navigate(['/']);
    }
  }

  /**
   * Attempts to create the question if all fields in the form are valid.
   */
  onSubmit(formData: FormData) {
    this.submittedOnce = false;
    if (this.questionForm.valid) {
      this.questionService.createNewQuestion(formData.title, formData.body).subscribe((question: Question) => {
        this.questionForm.reset();
        this.snackbarService.showSnackbar('Successfully submitted new question.');
        this.router.navigate(['/question', question.id]);
      }, (err) => {
        // display the error message from the api to the user.
        this.questionForm.setErrors({ api: err.error });
      });
    }
  }

}
