import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';

/**
 * This class provides utilities for workin with the api to create users.
 */
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient,
    private api: ApiService
  ) {}

  /**
   * Registers a new user with the given name and password.
   */
  createUser(name: string, password: string) {
    return this.http.post(this.api.getBaseUrl() + '/users', { name, password });
  }
}
