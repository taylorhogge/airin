import { Component, OnInit } from '@angular/core';
import { SnackbarService } from '../snackbar.service';

/**
 * This component works with the SnackbarService to provide a Snackbar
 * at the bottom of the screen that can be used to display quick
 * messages to the user.
 */
@Component({
  selector: 'app-snackbar',
  templateUrl: './snackbar.component.html',
  styleUrls: ['./snackbar.component.css']
})
export class SnackbarComponent implements OnInit {

  visible: boolean;
  message: string | null;

  constructor(
    private snackbarService: SnackbarService
  ) { }

  ngOnInit() {
    this.snackbarService.registerSnackbar(this);
    this.visible = false;
    this.message = null;
  }

  showSnackbar(message: string) {
    this.visible = true;
    this.message = message;
    setTimeout(() => {
      this.visible = false;
      this.message = null;
    }, 1500);
  }

}
