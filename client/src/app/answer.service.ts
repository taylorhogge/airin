import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SessionService } from './session.service';
import { ApiService } from './api.service';

export interface Answer {
  id: number;
  text: string;
  answerDate: string;
  authorName: string;
  questionId: number;
}

/**
 * This class interacts with the api in order to create and retreive answers.
 */
@Injectable({
  providedIn: 'root'
})
export class AnswerService {

  constructor(
    private apiService: ApiService,
    private sessionService: SessionService,
    private http: HttpClient
  ) {}

  /**
   * Retrieves all answers for the question with the given questionId.
   */
  getAnswersForQuestion(questionId: number) {
    return this.http.get<Answer[]>(this.apiService.getBaseUrl() + '/answers/forQuestion/' + questionId);
  }

  /**
   * Creates a new answer with the given text to the question with the given questionId.
   */
  createAnswer(text: string, questionId: number) {
    const headers = this.apiService.getSessionHeaders(this.sessionService.getSessionToken());
    return this.http.post<Answer>(this.apiService.getBaseUrl() + '/answers', { text, questionId }, { headers });
  }
}
