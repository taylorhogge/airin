import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';
import { SessionService } from './session.service';

export interface Question {
  id: number;
  title: string;
  body: string;
  publishDate: string;
  authorName: string;
}

/**
 * This class provides utilities for creating and retrieving questions with the api.
 */
@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  constructor(
    private http: HttpClient,
    private apiService: ApiService,
    private sessionService: SessionService
  ) { }

  /**
   * Creates a new question with the given title and body.
   * If the user is not logged in this function will through an exception.
   */
  createNewQuestion(title: string, body: string) {
    const headers = this.apiService.getSessionHeaders(this.sessionService.getSessionToken());
    return this.http.post<Question>(this.apiService.getBaseUrl() + '/questions', { title, body }, { headers });
  }

  /**
   * Searches all questions in the database with the given search phrase.
   * See https://www.postgresql.org/docs/9.1/datatype-textsearch.html for valid search phrase values.
   */
  searchQuestions(searchPhrase: string) {
    return this.http.get<Question[]>(this.apiService.getBaseUrl() + '/questions/search/' + searchPhrase);
  }

  /**
   * Gets all questions from the database.
   */
  getAllQuestions() {
    return this.http.get<Question[]>(this.apiService.getBaseUrl() + '/questions');
  }

  /**
   * Gets a single question by its id.
   */
  getQuestionById(id: number) {
    return this.http.get<Question>(this.apiService.getBaseUrl() + '/questions/' + id);
  }
}
