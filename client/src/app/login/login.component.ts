import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SessionService } from '../session.service';

interface FormData {
  username: string;
  password: string;
}

/**
 * This component defines the behavior of the "/login" page.
 * Provides a form for logging in a user.
 * Also links to the "/register" page for creating new users.
 * Redirects to the home page if the user is already logged in.
 */
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submittedOnce: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private sessionService: SessionService,
    private router: Router
  ) {
    this.loginForm = this.formBuilder.group({
      username: new FormControl(null, [Validators.required, Validators.minLength(4)]),
      password: new FormControl(null, [Validators.required, Validators.minLength(6)])
    });
    this.submittedOnce = false;
  }

  ngOnInit() {
    // redirect to home if the user is already logged in.
    if (this.sessionService.isLoggedIn()) {
      this.router.navigate(['/']);
    }
  }

  /**
   * Attempts to create the user if all form fields are valid.
   */
  onSubmit(formData: FormData) {
    this.submittedOnce = true;
    if (this.loginForm.valid) {
      this.sessionService.login(formData.username, formData.password).subscribe(() => {
        this.loginForm.reset();
        this.submittedOnce = false;
        this.router.navigate(['/']);
      }, (err) => {
        // display the error from the api to the user.
        this.loginForm.setErrors({ api: err.error });
      });
    }
  }

}
