import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

/**
 * The error that is thrown when attempting to get the current session token
 * or username when their is currently not logged in user.
 */
export class UnauthorizedError extends Error {}

interface LoginResponse {
  session_token: string;
}

/**
 * This class is responsible for managing the session of the currently logged in user.
 * Stores the username and session token in localStorage until logout so that our sessions
 * persist across different browser sessions.
 */
@Injectable({
  providedIn: 'root'
})
export class SessionService {

  private sessionToken: string | null;
  private username: string | null;

  constructor(private http: HttpClient, private api: ApiService) {
    this.sessionToken = localStorage.getItem('session');
    this.username = localStorage.getItem('username');
  }

  /**
   * Returns true if the current user is logged in.
   */
  public isLoggedIn(): boolean {
    return this.sessionToken !== null;
  }

  /**
   * Returns the session token of the currently logged in user.
   * If there is no logged in user, throws an UnauthorizedError.
   */
  public getSessionToken(): string {
    if (this.sessionToken === null) {
      throw new UnauthorizedError();
    } else {
      return this.sessionToken;
    }
  }

  /**
   * Returns the username of the currently logged in user.
   * If there is no logged in user, throws an UnauthorizedError.
   */
  public getUsername(): string {
    if (this.username === null) {
      throw new UnauthorizedError();
    } else {
      return this.username;
    }
  }

  /**
   * Attempts to log the user in by using the given username and password.
   * Subscribe to the observable to monitor success.
   */
  public login(username: string, password: string): Observable<LoginResponse> {
    const observable = this.http.post<LoginResponse>(this.api.getBaseUrl() + '/session', { username, password });
    return observable.pipe(
      tap((response: LoginResponse) => {
        this.sessionToken = response.session_token;
        this.username = username;
        localStorage.setItem('session', this.sessionToken);
        localStorage.setItem('username', this.username);
      })
    );
  }

  /**
   * Logs the currently logged in user out and deletes the active session from the database.
   */
  public async logout() {
    if (this.sessionToken !== null) {
      const headers = this.api.getSessionHeaders(this.sessionToken);
      const observable = this.http.delete(this.api.getBaseUrl() + '/session', { headers, responseType: 'text' });
      observable.subscribe(() => {
        this.sessionToken = null;
        this.username = null;
        localStorage.removeItem('session');
        localStorage.removeItem('username');
      });
    }
  }
}
