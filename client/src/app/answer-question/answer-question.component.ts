import { Input, Output, EventEmitter, Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Answer, AnswerService } from '../answer.service';

interface FormData {
  answer: string;
}

/**
 * This component provides a form for answering a question.
 * The id of the Question that this is responding to is passed in through the questionId slot.
 * An event called 'answered' emits the Answer that was created whenever a new answer is created using this form.
 */
@Component({
  selector: 'app-answer-question',
  templateUrl: './answer-question.component.html',
  styleUrls: ['./answer-question.component.css']
})
export class AnswerQuestionComponent implements OnInit {

  @Input()
  questionId: number;

  @Output()
  answered = new EventEmitter<Answer>();

  answerForm: FormGroup;

  // We keep this flag so that errors are not displayed until the user
  // attempts to submit the form at least once.
  submittedOnce: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private answerService: AnswerService,
  ) {
    this.answerForm = this.formBuilder.group({
      answer: new FormControl(null, [Validators.required])
    });
    this.submittedOnce = false;
  }

  ngOnInit() {
  }

  /**
   * Attempts to submit the form if all fields have passed validation.
   */
  onSubmit(formData: FormData) {
    this.submittedOnce = true;
    if (this.answerForm.valid) {
      this.answerService.createAnswer(formData.answer, this.questionId).subscribe((answer) => {
        this.answerForm.reset();
        this.submittedOnce = false;
        this.answered.emit(answer);
      }, (err) => {
        // display the error returned from the api to the user.
        this.answerForm.setErrors({ api: err.error });
      });
    }
  }

}
