import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { SessionService } from '../session.service';
import { SnackbarService } from '../snackbar.service';

interface FormData {
  username: string;
  password: string;
  passwordConfirm: string;
}

/**
 * This component defines the behavior of the "/register" page.
 * Provides a form for the user to register an account.
 * If the user is already logged in, redirects to home.
 */
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  submittedOnce: boolean;

  constructor(
    private userService: UserService,
    private sessionService: SessionService,
    private snackbarService: SnackbarService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.registerForm = this.formBuilder.group({
      username: new FormControl(null, [Validators.required, Validators.minLength(4)]),
      password: new FormControl(null, [Validators.required, Validators.minLength(6)]),
      passwordConfirm: new FormControl(null, [])
    }, {
      validator: (formGroup: FormGroup) => {
        if (formGroup.get('password').value !== formGroup.get('passwordConfirm').value) {
          return { nomatch: true };
        }
        return null;
      }
    });
    this.submittedOnce = false;
  }

  ngOnInit() {
    // redirect to home if the user is already logged in.
    if (this.sessionService.isLoggedIn()) {
      this.router.navigate(['/']);
    }
  }

  /**
   * Attempts to register the user with the api if all form fields are valid.
   */
  onSubmit(formData: FormData) {
    this.submittedOnce = true;
    if (this.registerForm.valid) {
      this.userService.createUser(formData.username, formData.password)
        .subscribe(() => {
          this.registerForm.reset();
          this.snackbarService.showSnackbar('Successfully registered! Please log in.');
          this.router.navigate(['/login']);
        }, (err) => {
          // display the error from the api to the user.
          this.registerForm.setErrors({ api: err.error });
        });
    }
  }

}
