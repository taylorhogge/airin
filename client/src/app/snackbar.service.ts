import { Injectable } from '@angular/core';
import { SnackbarComponent } from './snackbar/snackbar.component';

/**
 * This error is thrown if the SnackbarService is used
 * without an <app-snackbar> component existing on the page.
 */
export class MissingSnackbarError extends Error {}

/**
 * This class works together with the <app-snackbar> component
 * to display snackbars on the bottom of the page.
 */
@Injectable({
  providedIn: 'root'
})
export class SnackbarService {

  private component: SnackbarComponent | null;

  constructor() {
    this.component = null;
  }

  /**
   * Called by the <app-snackbar> component to register itself
   * with this service.
   */
  registerSnackbar(component: SnackbarComponent) {
    this.component = component;
  }

  /**
   * Shows the given message in the snackbar.
   * If there is no <app-snackbar> component on the page,
   * this will throw a MissingSnackbarError.
   */
  showSnackbar(message: string) {
    if (this.component == null) {
      throw new MissingSnackbarError();
    } else {
      this.component.showSnackbar(message);
    }
  }

}
