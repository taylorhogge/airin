import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Question } from '../question.service';
import { Answer, AnswerService } from '../answer.service';
import { SnackbarService } from '../snackbar.service';
import { SessionService } from '../session.service';

/**
 * This component defines the behavior of the "/question:id" page.
 * Displays details about the given question.
 */
@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

  question: Question;
  answers: Answer[];

  constructor(
    private activatedRoute: ActivatedRoute,
    private answerService: AnswerService,
    private snackbarService: SnackbarService,
    private sessionService: SessionService
  ) { }

  ngOnInit() {
    this.question = this.activatedRoute.snapshot.data.question;
    this.answerService.getAnswersForQuestion(this.question.id).subscribe((answers) => {
      this.answers = answers;
    }, (err) => {
      console.log(err);
    });
  }

  /**
   * This method responds to the 'answered' event emitted by the <app-answer-question> component.
   * Called every time the user submits a new answer for this question.
   * We insert the new answer into our list of answers and show a snackbar.
   */
  onAnswered(answer: Answer) {
    this.answers.push(answer);
    this.snackbarService.showSnackbar('Successfully submitted an answer.');
  }

  /**
   * Returns true if the user is currently logged in.
   */
  isLoggedIn(): boolean {
    return this.sessionService.isLoggedIn();
  }

}
