import { TestBed } from '@angular/core/testing';

import { QuestionResolverService } from './question-resolver.service';

describe('QuestionResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: QuestionResolverService = TestBed.get(QuestionResolverService);
    expect(service).toBeTruthy();
  });
});
