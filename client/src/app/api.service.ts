import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

/**
 * This class provides som utilities for working with the api.
 */
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor() { }

  /**
   * Returns the base url to the api.
   */
  public getBaseUrl(): string {
    return 'http://localhost:8080';
  }

  /**
   * Creates the AIRIN_SESSION header with the given session token.
   */
  public getSessionHeaders(sessionToken: string): HttpHeaders {
    return new HttpHeaders().set('AIRIN_SESSION', sessionToken);
  }
}
