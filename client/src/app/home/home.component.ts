import { Component, OnInit } from '@angular/core';
import { Question, QuestionService } from '../question.service';

/**
 * This component defines the behaviour of the home page.
 * Displays a list of all questions and a search bar to filter them.
 */
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  searchTerm: string;
  results: Question[];

  constructor(
    private questionService: QuestionService
  ) { }

  ngOnInit() {
    this.searchTerm = '';
    this.getAllQuestions();
  }

  /**
   * This method is called every time the search term changes.
   * If there is no search term: load all questions.
   * If there is a search term: convert it to a tsquery and send to the database.
   */
  searchChanged() {
    if (this.searchTerm.trim().length === 0) {
      return this.getAllQuestions();
    }

    const searchPhrase = this.buildSearchQuery(this.searchTerm);
    this.questionService.searchQuestions(searchPhrase)
      .subscribe((questions: Question[]) => {
        this.results = questions;
      }, (err) => {
        console.log(err);
      });
  }

  /**
   * Loads all questions from the database.
   */
  private getAllQuestions() {
    this.questionService.getAllQuestions()
      .subscribe((questions: Question[]) => {
        this.results = questions;
      }, (err) => {
        console.log(err);
      });
  }

  /**
   * Constructs a tsquery compatible search phrase from the value in the search box.
   * See https://www.postgresql.org/docs/9.1/datatype-textsearch.html for more.
   * Uses a prefixed keyword search:
   * ie turns "keyword keyw" into "keyword:* | keyw:*"
   */
  private buildSearchQuery(searchTerm: string): string {
    const terms = searchTerm.trim().split(' ');
    if (terms.length === 1) {
      return terms[0] + ':*';
    }
    return terms.reduce((phrase, term, idx) => {
      if (idx === 0) {
        return term;
      }
      return phrase + ' | ' + term + ':*';
    }, '');
  }

}
