import "reflect-metadata";
import {createConnection} from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import * as cors from "cors";
import {User} from "./entity/User";
import {Session} from "./entity/Session";
import {Question} from "./entity/Question";
import {Answer} from "./entity/Answer";
import {UserController} from "./controller/UserController";
import {SessionController} from "./controller/SessionController";
import {QuestionController} from "./controller/QuestionController";
import {AnswerController} from "./controller/AnswerController";

// Load environment variables from .env file
if (process.env.DEV_LEVEL !== "prod") {
    require("dotenv").config();
}

createConnection({
    type: "postgres",
    host: process.env.DB_HOST || "localhost",
    port: +process.env.DB_PORT || 5432,
    username: process.env.DB_USER || "admin",
    password: process.env.DB_PASS || "admin",
    database: process.env.DB_NAME || "airin",
    entities: [
        User,
        Session,
        Question,
        Answer
    ],
    synchronize: true,
    logging: true
}).then(async connection => {

    // create express app
    const app = express();
    app.use(cors());
    app.use(bodyParser.json());
    
    // setup express routes
    app.use("/users", new UserController(connection).getRoutes());
    app.use("/", new SessionController(connection).getRoutes());
    app.use("/questions", new QuestionController(connection).getRoutes());
    app.use("/answers", new AnswerController(connection).getRoutes());

    // start express server
    const port = process.env.SERVER_PORT || 8080;
    app.listen(port);
    console.log(`Server listening on port ${port}.`);

}).catch(error => console.log("Error connecting to database: ", error));
