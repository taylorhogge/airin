import {Entity, PrimaryColumn, Column, OneToMany, OneToOne} from "typeorm";
import {Question} from './Question';
import {Answer} from './Answer';
import {Session} from './Session';

/**
 * This class defines the database schema for a user.
 * The database table is named "users" instead of "user" because
 * the "user" table is used by postgres to manage actual database user accounts.
 */
@Entity("users")
export class User {
    
    /**
     * The username of this user. Required to be unique.
     */
    @PrimaryColumn()
    name: string;
    
    /**
     * The password hash of this user. Used to validate logins.
     * Generated with the argon2 hashing algorithm.
     */
    @Column()
    password_hash: string;
    
    @OneToMany(type => Question, question => question.author)
    questions: Promise<Question[]>;
    
    @OneToMany(type => Answer, answer => answer.author)
    answers: Promise<Answer[]>
    
    @OneToOne(type => Session, session => session.user)
    session: Promise<Session>

}
