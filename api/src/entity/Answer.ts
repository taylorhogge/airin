import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, CreateDateColumn, JoinColumn} from "typeorm";
import {Question} from './Question';
import {User} from './User';

/**
 * This class defines the database schema for answers.
 */
@Entity()
export class Answer {
    
    /**
     * The unique id of this answer.
     */
    @PrimaryGeneratedColumn()
    id: number;
    
    /**
     * The text content of this answer.
     */
    @Column()
    text: string;
    
    /**
     * The date that this answer was created.
     */
    @CreateDateColumn()
    answerDate: Date;
    
    /**
     * The name of the user who created this answer.
     */
    @Column()
    authorName: string;

    /**
     * The id of the question that this answer refers to.
     */
    @Column()
    questionId: number;
    
    @ManyToOne(type => User, user => user.answers, {
        nullable: false
    })
    @JoinColumn()
    author: Promise<User>;
    
    @ManyToOne(type => Question, question => question.answers, {
        nullable: false
    })
    @JoinColumn()
    question: Promise<Question>;
    
}
