import {Entity, JoinColumn, PrimaryGeneratedColumn, OneToOne, Column} from "typeorm";
import {User} from "./User";

/**
 * This class defines the database schema for an active user session.
 */
@Entity()
export class Session {
    
    /**
     * The unique id for this session.
     * It is important to not use a sequential id here
     * so that other user's session tokens cannot be inferred
     * from others.
     */
    @PrimaryGeneratedColumn('uuid')
    id: string;
    
    /**
     * The user that is logged in.
     */
    @Column()
    userName: string;
    
    @OneToOne(type => User, user => user.session, {
        nullable: false
    })
    @JoinColumn()
    user: Promise<User>;

}
