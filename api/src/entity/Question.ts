import {Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne, CreateDateColumn, JoinColumn} from "typeorm";
import {Answer} from './Answer';
import {User} from './User';

/**
 * This class defines the database schema for questions.
 */
@Entity()
export class Question {

    /**
     * The unique id of this question.
     */
    @PrimaryGeneratedColumn()
    id: number;

    /**
     * The title of this question.
     */
    @Column()
    title: string;

    /**
     * The body of this question.
     */
    @Column()
    body: string;
    
    /**
     * The date that this question was asked.
     */
    @CreateDateColumn()
    publishDate: Date;
    
    /**
     * The user that asked this question.
     */
    @Column()
    authorName: string;
    
    @ManyToOne(type => User, user => user.questions, {
        nullable: false
    })
    @JoinColumn()
    author: Promise<User>;
    
    @OneToMany(type => Answer, answer => answer.question, {
        cascade: true
    })
    answers: Promise<Answer[]>;
    
}
