import {Connection, Repository} from "typeorm";
import {Router, Request, Response} from "express";
import {Question} from "../entity/Question";
import {AuthorizedRequest, Authenticator} from "../middleware/Authenticator";

/**
 * This class defines the http services for manipulating Questions.
 */
export class QuestionController {
    
    private questionRepository: Repository<Question>; 
    private authenticator: Authenticator;
    
    constructor(connection: Connection) {
        this.questionRepository = connection.getRepository(Question);
        this.authenticator = new Authenticator(connection);
    }
    
    /**
     * Defines the express routes for these services.
     */
    getRoutes(): Router {
        return Router()
            .post("/", this.authenticator.requireLogin, this.createQuestion)
            .get("/", this.getAllQuestions)
            .get("/:id", this.getQuestionById)
            .get("/askedBy/:user", this.getQuestionsByUser)
            .get("/search/:phrase", this.searchQuestions)
            .delete("/:id", this.authenticator.requireLogin, this.deleteQuestionById)
    }
    
    /**
     * POST "/"
     * Creates a new question.
     * Requires the user to be logged in.
     * Requires a json body with the following fields:
     * {
     *   title: string;
     *   body: string;
     * }
     */
    createQuestion = async (req: AuthorizedRequest, res: Response) => {
        if (!req.body) {
            return res.sendStatus(400);
        }
        
        if (!req.body.title || typeof req.body.title !== "string") {
            return res.status(400).send("title is required");
        }
        
        if (!req.body.body || typeof req.body.body !== "string") {
            return res.status(400).send("body is required");
        }
        
        const title: string = req.body.title;
        const body: string = req.body.body;
        
        if (title.trim().length === 0) {
            return res.status(400).send("title must not be empty");
        }
        if (body.trim().length === 0) {
            return res.status(400).send("body must not be empty");
        }
        
        let question = new Question();
        question.title = title;
        question.body = body;
        question.authorName = req.session.userName;
        
        await this.questionRepository.insert(question);
        res.json(getQuestionDto(question));
    }
    
    /**
     * GET "/"
     * Retrieve all questions in the database.
     */
    getAllQuestions = async (req: Request, res: Response) => {
        const questions = await this.questionRepository.find();
        res.json(questions.map(getQuestionDto));
    }
    
    /**
     * GET "/:id"
     * Retrieve a single question by its id.
     */
    getQuestionById = async (req: Request, res: Response) => {
        const question = await this.questionRepository.findOne(req.params.id);
        if (!question) {
            return res.sendStatus(404);
        }
        res.json(getQuestionDto(question));
    }
    
    /**
     * GET "/askedBy/:user"
     * Retrieve all the questions asked by the given user.
     */
    getQuestionsByUser = async (req: Request, res: Response) => {
        const questions = await this.questionRepository.find({ authorName: req.params.user });
        res.json(questions.map(getQuestionDto));
    }
    
    /**
     * GET "/search/:phrase"
     * Searches all questions in the database using the given search phrase.
     * The search phrase is parsed as a tsquery.
     * See https://www.postgresql.org/docs/9.1/datatype-textsearch.html
     * for possible search phrase values.
     */
    searchQuestions = async (req: Request, res: Response) => {
        // This search is not indexed but for the amount of data that this small project
        // is using this quick and dirty query works well enough.
        const searchPhrase = req.params.phrase;
        const results = await this.questionRepository
            .createQueryBuilder("question")
            .orWhere("to_tsvector(question.title) @@ to_tsquery(:searchPhrase)", { searchPhrase })
            .orWhere("to_tsvector(question.body) @@ to_tsquery(:searchPhrase)", { searchPhrase })
            .getMany();
        res.json(results.map(getQuestionDto));
    }
    
    /**
     * DELETE "/:id"
     * Deletes the question with the given id from the database.
     * Requires the user to be logged in and that they were the one that asked the question..
     */
    deleteQuestionById = async (req: AuthorizedRequest, res: Response) => {
        const deleteResult = await this.questionRepository.delete(req.params.id);
        if (deleteResult.affected == 0) {
            res.status(400).send(`No question exists with id: ${req.params.id}`);
        } else {
            res.send("Success");
        }
    }
    
}

function getQuestionDto(question: Question) {
    return {
        id: question.id,
        title: question.title,
        body: question.body,
        publishDate: question.publishDate,
        authorName: question.authorName
    };
}
