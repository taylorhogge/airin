import {Connection, Repository} from "typeorm";
import {Router, Request, Response} from "express";
import {Answer} from "../entity/answer";
import {AuthorizedRequest, Authenticator} from "../middleware/Authenticator";

/**
 * This class defines the http services that create and retrieve answers.
 */
export class AnswerController {
    
    private answerRepository: Repository<Answer>;
    private authenticator: Authenticator;
    
    constructor(connection: Connection) {
        this.answerRepository = connection.getRepository("Answer");
        this.authenticator = new Authenticator(connection);
    }
    
    /**
     * Defines the express routes for each service implemented by this class.
     */
    getRoutes(): Router {
        return Router()
            .post("/", this.authenticator.requireLogin, this.createAnswer)
            .get("/:id", this.getAnswerById)
            .delete("/:id", this.authenticator.requireLogin, this.deleteAnswer)
            .get("/forQuestion/:id", this.getAnswersForQuestion)
    }
    
    /**
     * POST "/"
     * Create a new answer.
     * Requires the user to be logged in.
     * Requires a json body with fields 'text' and 'questionId'
     */
    createAnswer = async (req: AuthorizedRequest, res: Response) => {
        if (!req.body) {
            return res.sendStatus(400);
        }
        
        if (!req.body.text || typeof req.body.text !== "string") {
            return res.status(400).send("text is required");
        }
        
        if (!req.body.questionId || typeof req.body.questionId !== "number") {
            return res.status(400).send("questionId is required");
        }
        
        const text: string = req.body.text;
        const questionId: number = req.body.questionId;
        
        if (text.trim().length == 0) {
            return res.status(400).send("text must not be empty");
        }
        
        let answer = new Answer();
        answer.text = text;
        answer.questionId = questionId;
        answer.authorName = req.session.userName;
        await this.answerRepository.save(answer);
        
        res.json(getAnswerDto(answer));
    }
    
    /**
     * GET "/:id"
     * Retrieves an answer by its id.
     */
    getAnswerById = async (req: Request, res: Response) => {
        const answer = await this.answerRepository.findOne(req.params.id);
        if (!answer) {
            return res.sendStatus(404);
        }
        res.json(getAnswerDto(answer));
    }
    
    /**
     * GET "/forQuestion/:id"
     * Retrieves all answers for the question with :id.
     */
    getAnswersForQuestion = async (req: Request, res: Response) => {
        const answers = await this.answerRepository.find({ questionId: req.params.id });
        res.json(answers.map(getAnswerDto));
    }
    
    /**
     * DELETE "/:id"
     * Deletes the answer with :id
     * Requires that the user is logged in and that they are the author
     * of the answer to be deleted.
     */
    deleteAnswer = async (req: AuthorizedRequest, res: Response) => {
        const answer = await this.answerRepository.findOne(req.params.id);
        if (!answer) {
            return res.status(400).send(`Answer with id: ${req.params.id} does not exist.`);
        }
        if (answer.authorName !== req.session.userName) {
            return res.sendStatus(401);
        }
        this.answerRepository.delete(answer.id);
        res.send("Success");
    }
}

function getAnswerDto(answer: Answer) {
    return {
        id: answer.id,
        text: answer.text,
        answerDate: answer.answerDate,
        authorName: answer.authorName,
        questionId: answer.questionId
    };
}
