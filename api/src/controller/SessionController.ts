import {Connection, Repository} from "typeorm";
import {Router, Request, Response} from "express";
import * as argon2 from "argon2";
import {Session} from "../entity/Session";
import {User} from "../entity/User";
import {Authenticator, AuthorizedRequest} from "../middleware/Authenticator";

/**
 * This class defines the http services used for manipulating user sessions.
 */
export class SessionController {
    private sessionRepository: Repository<Session>;
    private userRepository: Repository<User>;
    private authenticator: Authenticator;
    
    constructor(connection: Connection) {
        this.sessionRepository = connection.getRepository(Session);
        this.userRepository = connection.getRepository(User);
        this.authenticator = new Authenticator(connection);
    }
    
    /**
     * Defines the express routes implemented by this controller.
     */
    getRoutes(): Router {
        return Router()
            .post("/session", this.login)
            .delete("/session", this.authenticator.requireLogin, this.logout);
    }
    
    /**
     * POST "/session"
     * Logs the user in after validating their username and password.
     * Requires a json body with the fields:
     * {
     *   username: string;
     *   password: string;
     * }
     * On successful login, responds with a json object containing the field
     * session_token which contains their session identifier.
     * This token is found by the Authenticator by looking in the AIRIN_SESSION request header.
     */
    login = async (req: Request, res: Response) => {
        if (!req.body) {
            return res.sendStatus(400);
        }
        
        if (!req.body.username || !req.body.password) {
            return res.status(400).send("username or password is missing");
        }
        
        if (typeof req.body.username !== "string" || typeof req.body.password !== "string") {
            return res.status(400).send("username and password must be a string");
        }
        
        const username: string = req.body.username;
        const password: string = req.body.password;
        
        const user = await this.userRepository.findOne(username);
        if (!user) {
            return res.status(400).send(`A user with the name ${username} does not exist.`);
        }
        
        if (!await argon2.verify(user.password_hash, password)) {
            return res.status(400).send("Incorrect password.");
        }
        
        const existingSession = await this.sessionRepository.findOne({userName: username});
        if (existingSession) {
            return res.json({
                session_token: existingSession.id
            });
        } else {
            const session = new Session();
            session.userName = username;
            await this.sessionRepository.insert(session);
            return res.json({
                session_token: session.id
            });
        }
    }
    
    /**
     * DELETE "/session"
     * Requires the user to be logged in.
     * Deletes the currently active session from the database forcing all current sessions
     * for the current user to be invalidated.
     */
    logout = async (req: AuthorizedRequest, res: Response) => {
        await this.sessionRepository.delete(req.session.id);
        res.send("Success")
    }
}
