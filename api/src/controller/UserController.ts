import {Connection, Repository} from "typeorm";
import {Router, Request, Response} from "express";
import * as argon2 from "argon2";
import {User} from "../entity/User";

/**
 * This class defines the http services for manipulating users.
 */
export class UserController {
    
    private userRepository: Repository<User>;
    
    constructor(connection: Connection) {
        this.userRepository = connection.getRepository(User);
    }
    
    /**
     * Defines the express routes implemented by this controller.
     */
    getRoutes(): Router {
        return Router()
            .get("/:name", this.getUserByName)
            .post("/", this.createUser);
    }

    /**
     * GET "/:name"
     * Retreives the user with the given name.
     */
    getUserByName = async (req: Request, res: Response) => {
        const user = await this.userRepository.findOne(req.params.name);
        if (!user) {
            res.sendStatus(404);
        }
        res.json({
            name: user.name
        });
    }

    /**
     * POST "/"
     * Registers a new user.
     * Requires a json body with the following fields:
     * {
     *   name: string;
     *   password: string;
     * }
     */
    createUser = async (req: Request, res: Response) => {
        if (!req.body) {
            return res.sendStatus(400);
        }
        
        if (!req.body.name || !req.body.password) {
            return res.status(400).send("name or password missing.");
        }
        
        if (typeof req.body.name !== "string" || typeof req.body.password !== "string") {
            return res.status(400).send("name and password must be strings");
        }
        
        const name: string = req.body.name;
        const password: string = req.body.password;
        
        if (name.length < 4 || name.length > 12) {
            return res.status(400).send("Name must be between 4 and 12 characters");
        }
        
        if (password.length < 6) {
            return res.status(400).send("Password must be at least 6 characters");
        }

        let user = new User();
        user.name = name;
        user.password_hash = await argon2.hash(password);

        try {
            await this.userRepository.insert(user);
            res.json({
                name
            });
        } catch {
            res.status(400).send(`A user with the name ${name} already exists.`);
        }
    }

}
