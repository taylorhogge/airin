import { Connection, Repository } from "typeorm";
import { NextFunction, Request, Response } from "express";
import { Session } from "../entity/Session";

/**
 * An HTTP Request that has been authenticated by the Authenticator.
 */
export interface AuthorizedRequest extends Request {
    session: Session;
}

/**
 * This class defines middleware that validates active sessions.
 */
export class Authenticator {
    
    private static SESSION_HEADER = "AIRIN_SESSION";
    private sessionRepository: Repository<Session>;
    
    constructor(connection: Connection) {
        this.sessionRepository = connection.getRepository(Session);
    }
    
    /**
     * Requires that this request has an active session attached to it.
     * Looks for a session token in the AIRIN_SESSION header.
     * If no session token is found, or the session token refers to a stale
     * session, a 401 Unauthorized status is returned and the next function is not called.
     * If the session is valid, it is attached to the request at Request.session
     */
    requireLogin = async (req: Request, res: Response, next: NextFunction) => {
        const sessionId = req.get(Authenticator.SESSION_HEADER);
        if (!sessionId) {
            return res.sendStatus(401);
        }
        const session = await this.sessionRepository.findOne(sessionId);
        if (!session) {
            return res.sendStatus(401);
        }
        (req as AuthorizedRequest).session = session;
        next();
    }
    
}
